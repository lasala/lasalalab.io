axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
css_pipeline = require 'css-pipeline'
records      = require 'roots-records'
marked       = require 'marked'
strftime     = require 'strftime'
jeet         = require 'jeet'
dynamic      = require './dynamic'

print = console.log

json = (what) ->
  JSON.stringify(what, null, 2)

slugify = (text) ->
  text.toLowerCase()
    .replace(/[^\w\s-\/]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '')

String::slugify = ->
  slugify(@)

data_url = (part) ->
  "https://classroom-3653a.firebaseio.com/#{part}.json"

module.exports =
  ignores: ['README.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf']

  extensions: [
    js_pipeline(
      # files: '/assets/js/**'
      files: '/assets/js/turbolinks.js'
      # out: '/js/build.js'
    )
    css_pipeline(files: 'assets/css/*.styl', out: '/css/build.css')
    records(
      classes:
        url: data_url 'classes'
        hook: (data) ->
          # print 'classes hook running'
          periods = Object.keys(data).map (key) ->
            data[key]
          periods
        # template: 'views/_period.jade',
        # out: (period) -> "/classes/#{period.about.name.slugify()}"

      subjects:
        url: data_url 'subjects'
        hook: (data) ->
          # print 'subjects hook running'
          data

      assignments:
        url: data_url 'assignments'
    )
    dynamic(
      hook: (locals) ->
        print 'dynamic is running!!!'
        records = locals.records
        new_pages = []
        if records?
          for period in records['classes']
            calendar = period.calendar
            subject = period.about.subject
            new_pages.push
              path: "/classes/#{period.about.name.slugify()}"
              template: 'views/_period.jade'
              locals: period

            for assignment, data of records['assignments'][subject]
              due_date = period
              new_pages.push
                path: "classes/#{period.about.name.slugify()}/#{assignment.slugify()}"
                template: 'views/_assignment.jade'
                locals:
                  subject: subject
                  assignment: assignment
                  assignment_data: data
          new_pages
        else
          console.error 'Records doesn\'t exist'

    )
				
    # dynamic_content()
  ]

  live_reload: false

  browsersync:
    snippetOptions:
      rule:
        match: /<\/head>/i,
        fn: (snippet, match) ->
          return snippet + match

    ui:
      port: 8081

  server:
    clean_urls: true

  stylus:
    use: [jeet(), rupture(), autoprefixer(), axis()]
    sourcemap: true

  'coffee-script':
    sourcemap: true

  locals:
    markdown: marked
    strftime: strftime
    slugify: slugify
    assignment_url: (subject, assignment) ->
      "/assignments/#{slugify(subject)}/#{slugify(assignment)}"
