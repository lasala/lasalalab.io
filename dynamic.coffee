fs        = require 'fs'
rest      = require 'rest'
path      = require 'path'
When         = require 'when'
node      = require 'when/node'
_         = require 'lodash'
RootsUtil = require 'roots-util'

module.exports = (opts) ->

  class Dynamic

    ###*
     * Creates a locals object if one isn't set.
     * @constructor
     ###

    constructor: (@roots) ->
      @util = new RootsUtil(@roots)
      @roots.config.locals ||= {}
      @roots.config.locals.dynamic ||= {}
      @new_pages = []

    ###*
     * Setup extension method loops through objects and
     * returns a promise to get all data and store.
     ###

    setup: ->
      # @new_pages = opts.hook(@proxy)
      # console.log 'Running Setup'
      # console.log 'Done with Setup, now running proxies'

    fs: ->
      category: 'all'
      detect: (f) ->
        true

    category_hooks: ->
      after: (context, category) ->
        # console.log 'running category_hooks'
        @roots = context.roots
        @util = new RootsUtil(@roots)
        @new_pages = opts.hook(@roots.config.locals)
        # console.log JSON.stringify @new_pages
        for page in @new_pages

          if page.template?
            tpl = page.template
          else
            console.error 'You must provide a template'

          if page.path?
            out = @roots.config.locals.slugify(page.path)
          else
            console.error 'You must provide an output path'

          tpl_path = path.join(@roots.root, tpl)
          output_path = "#{out}.html"
          # console.log "configuring #{output_path} with #{JSON.stringify(page.locals)}"
          compiler = _.find @roots.config.compilers, (c) ->
            _.contains(c.extensions, path.extname(tpl_path).substring(1))
          compiler_opts = {}
          compiler_opts = _.extend(
            @roots.config.locals,
            @roots.config[compiler.name] ? {},
            _path: output_path,
            { item: page.locals }
          )
          # console.log JSON.stringify(compiler_opts)
          render_file(tpl_path, compiler,  compiler_opts, output_path)


    ###*
     * Given a records object, if that object has `template` and `out` keys, and
     * its data is an array, iterates through its data, creating a single view
     * for each item in the array using the template provided in the `template`
     * value, and writing to the path provided in the `out` value.
     *
     * @param {Object} obj - record object with a `key`, `options`, and `data`
    ###

    render_file = (tpl_path, compiler, compiler_options, output_path) ->
      # console.log "about to render #{output_path}"
      compiler.renderFile(tpl_path, compiler_options)
        .then (res) =>
          # console.log "Rendered #{output_path}"
          @util.write(output_path, res.result)
      # compiler.renderFile(tpl_path, compiler_options, (res) ->
      #   console.log "done rendering #{output_path}"
      #   @util.write(output_path, res.result)
      # )
