cd public
for file in $(find -iname "*.html")
do
  if [ "$file" != "./index.html" ]
  then
    name=$(dirname $file)/$(basename $file .html)
    mkdir -p $name
    echo "$file -> $name/index.html"
    mv $file $name/index.html
  fi
done
cd ..
