# [classroom.gitlab.io](https://classroom.gitlab.io)

This is a platform for teacher websites, with content built with [Middleman](https://middlemanapp.com/).
